import express from "express"
import cors from "cors"
import { db } from "./dbConn.js"
import cookieParser from "cookie-parser";
import jwt from "jsonwebtoken";
import { fileURLToPath } from 'url';
import { dirname } from 'path';
import path from 'path';

const __filename = fileURLToPath(import.meta.url);
const __dirname = dirname(__filename);
const app = express();
 
app.use(express.static(path.join(__dirname, 'build')));
app.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', '*'); // Replace with your desired origin or '*' for any origin

  // Set the allowed HTTP methods
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE');

  // Set the allowed headers
  res.setHeader('Access-Control-Allow-Headers', 'Content-Type, Authorization');

  // Allow credentials if needed
  res.setHeader('Access-Control-Allow-Credentials', 'true');

  // Continue to the next middleware
  next();
  });  
app.get('/', (req, res) => {
    res.sendFile(path.join(__dirname, 'build', 'index.html'));
});
app.use(express.json());
app.use(cookieParser());


app.get("/posts", (req,res)=>{
    db.query("SELECT * FROM post WHERE author_id <> ? ORDER BY idpost DESC",req.query.id,(err,result)=>{
        if (err) return res.json(err);
        return res.json(result);
    })
});
app.get("/comments", (req,res)=>{
    db.query("SELECT * FROM comment WHERE  post_id = ? ORDER BY idComment DESC",req.query.postId,(err,result)=>{
        if (err) return res.json(err);
        return res.json(result);
    })
});
app.get("/user", (req,res)=>{
    db.query("SELECT * FROM user WHERE  id = ?",req.query.id,(err,result)=>{
        if (err) {return res.json(err)}
        else {
            return res.json(result);
        }
    });
});
app.get("/request", (req,res)=>{
    db.query("SELECT * FROM requests WHERE toId = ? ",req.query.data,(err,result)=>{
        if (err) return res.json(err);
        return res.json(result);
    })
});
app.get("/accept", (req,res)=>{
    db.query("SELECT * FROM requests WHERE toId = ?",req.query.data,(err,result)=>{
        if (err) return res.json(err);
        return res.json(result);
    })
});
app.use("/check", (req,res)=>{
    
    db.query("SELECT * FROM user WHERE username = ?",[req.body.username],(err,result)=>{
        if(err) {
            return res.json(err);
        }
        if(result.length === 0) {
            console.log("wtf");
            return res.status(404).json("user doesn't exist");
        }
        //check password
        if(result[0].password != req.body.password){
            return res.status(404).json("wrong password");
        }
        //return res.json("enter");
        const token = jwt.sign({ id: result[0].id },"yamamotogenryuusaishigekuni");

        const {password, ...others} = result[0];

        res.cookie("accessToken", token, {
            httpOnly:true,
        }).status(200).json({...others,token});
        });
    });
    //return res.json(req.body);
app.use("/register", (req,res)=>{
    db.query("SELECT * FROM user WHERE username = ?",req.body.username,(err,data)=>{
        if(err) return res.json(err);
        if(data.length==1){
            return res.status(404).json("username is taken");
        }
        if(req.body.password != req.body.confirm){
            return res.status(404).json("passwords didn't match");
        }
        const creds = [req.body.username, req.body.password, "registered"];
        db.query("INSERT INTO user (`username`, `password`, `role`) VALUES (?)", [creds] ,(err1,data1)=>{
            if(err1) return res.json(err1);
            else return res.json(data1);
        });
    });
});
app.use("/deletepart", (req,res)=>{
    db.query("DELETE FROM partnership WHERE idPartnership = ?",[req.body.id],(err,data)=>{
       if(err) res.json(err);
       res.json(data); 
    });
});
app.use("/logout", (req,res) =>{
    res.clearCookie("accessToken",{
        secure:true,
        sameSite:"none"
      }).status(200).json("User has been logged out.")
});
app.use("/partnership", (req,res) =>{
    db.query("INSERT INTO partnership ( `user_id`, `partner_id`) VALUES (?)",[[req.body.uid,req.body.pid]],(err,data)=>{
        if(err) return res.json(err);
        else return res.json(data);
    });
});
app.use("/removereq", (req,res) =>{
    db.query("DELETE FROM requests WHERE toId = ? AND fromId = ?",[req.body.toid,req.body.fromid],(err,data)=>{
        if(err) return res.json(err);
        else return res.json(data);
    })
});
app.get("/myposts", (req,res) =>{
    db.query("SELECT * FROM post WHERE author_id = ? ORDER BY idpost DESC",req.query.id,(err,data)=>{
        if(err) return res.json(err);
        else return res.json(data);
    })
    
});
app.get("/partner", (req,res) =>{
    db.query("SELECT * FROM partnership WHERE user_id = ? OR partner_id = ?",[req.query.id,req.query.id],(err,data)=>{
        if(err) return res.json(err);
        else return res.json(data);
    })
    
});
app.get("/search", (req,res) =>{

    const query = "%"+req.query.data+"%";
    const query1 = req.query.data+"%"; 
    console.log(query);
    const r1 = "SELECT * FROM post WHERE shortDescription LIKE ? ORDER BY idpost DESC";
    const r2 = "SELECT * FROM user WHERE username LIKE ?";
    const r3 = "SELECT * FROM post WHERE topic LIKE ? ORDER BY idpost DESC";
    const promise1 = new Promise((resolve, reject) => {
        db.query(r1, query, (error, results) => {
          if (error) {
            console.log(error);
            reject(error);
          } else {
            resolve(results);
          }
        });
      });
      
      const promise2 = new Promise((resolve, reject) => {
        db.query(r2, query1, (error, results) => {
          if (error) {
            console.log(error);
            reject(error);
          } else {
            resolve(results);
          }
        });
      });
      
      const promise3 = new Promise((resolve, reject) => {
        db.query(r3, query1, (error, results) => {
          if (error) {
            console.log(error);
            reject(error);
          } else {
            resolve(results);
          }
        });
      });
      
      Promise.all([promise1, promise2, promise3])
        .then((results) => {
          res.json(results);
        })
        .catch((error) => {
          // Handle error
          console.error(error);
          res.status(500).json({ error: "Internal Server Error" }); 
        });
    });
app.use("/comment", (req,res) =>{
    db.query("INSERT INTO comment (`post_id`, `author_id`, `content`) VALUES (?)", [[req.body.post, req.body.user, req.body.comment]] ,(err,data)=>{
        if(err) return res.json(err);
        else return res.json(data);
    });   
});
app.use("/addpost",(req,res)=>{
 
    db.query("INSERT INTO post (`topic`, `author_id`,`shortDescription`, `content`,`type`) VALUES (?)",[[req.body.topic, req.body.author_id,req.body.short, req.body.content, req.body.type]],(err,data)=>{
        if(err) return res.json(err);
        else return res.json(data);
    })
    
})
app.use("/ent", (req,res)=>{
    db.query("SELECT * from user",(err,data)=>{
        if(err) return res.json(err);
        return res.json(data);
    })
});
app.use("/requests", (req,res)=>{
    console.log(req.body);
    db.query("INSERT INTO requests ( `toId`, `fromId`) VALUES (?)",[[req.body[0],req.body[1]]],(err,data)=>{
        if(err) return res.json(err);
        return res.json(data);
    })
});
app.listen(5051, ()=>{
    console.log("alo 1bre");
});