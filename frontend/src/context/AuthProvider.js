import { createContext, useState, useEffect } from "react";

export const AuthContext = createContext({});

export const AuthProvider = ({ children }) => {
    const [childCount, setChildCount] = useState();
    const [global,setGlobal] = useState(true);
    const [available, setAvailable] = useState([]);
    const [auth, setAuth] = useState(JSON.parse(localStorage.getItem("user")) || null);
    const [post, setPost] = useState(null);
    const [user, setUser] = useState(null);
    useEffect(() => {
        localStorage.setItem("user", JSON.stringify(auth));
      }, [auth]);
    return (
        <AuthContext.Provider value={{ childCount, setChildCount, global, setGlobal, auth, setAuth,post,setPost,user,setUser, available, setAvailable }}>
            {children}
        </AuthContext.Provider>
    )
}

export default AuthContext;