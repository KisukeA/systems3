import './App.css';
import {BrowserRouter, Routes, Route } from "react-router-dom";
import Welcome from "./components/Welcome.js";
import Signup from "./components/Signup.js";
import Login from "./components/Login.js";
import About from "./components/About.js";
import Home from "./components/Home.js";
import Logout from "./components/Logout.js";
import Profile from './components/Profile.js';
import AuthContext from './context/AuthProvider';
import SinglePost from './components/SinglePost.js';
import SingleUser from './components/SingleUser.js';
import { useContext } from 'react';
import Search from './components/Search';
import ShowPartnerships from './components/ShowPartnerships.js';

function App() {
  const { auth, post } = useContext(AuthContext); 
  return (
    <div className="App">
      <BrowserRouter>
      <Routes>
        <Route path = "/" element={<Welcome/>}></Route>
        <Route path = "/signup" element={<Signup/>}></Route>
        <Route path = "/login" element={<Login/>}></Route>
        <Route path = "/home" element = {<Home/>}></Route>
        <Route path = "/about" element = {<About/>}></Route>
        <Route path = "/profile" element = {<Profile />}></Route>
        <Route path = "/search" element = {<Search/>}></Route>
        <Route path = "/singlepost" element = {<SinglePost post = {post}/>}></Route>
        <Route path = "/singleuser" element = {<SingleUser/>}></Route>
        <Route path = "/showpart" element = {<ShowPartnerships/>}></Route>
        <Route path = "/logout" element = 
          {
            <div>
              <Logout/>
              <Welcome/>
            </div>
          }>
        </Route>
      </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
