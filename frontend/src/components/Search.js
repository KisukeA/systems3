import React, { useContext } from 'react';
import './Search.css';
import  { useState } from "react";
import {Link,useNavigate} from "react-router-dom"
import User from "./User.js";
import Postie from "./Postie.js";
import AuthContext from '../context/AuthProvider';

const Search = ({searchResult}) => {
    const { setPost } = useContext(AuthContext);
    const { setUser } = useContext(AuthContext);
    const navigate = useNavigate();
    const [selectedOption, setSelectedOption] = useState("1");
    const [showUsers, setShowUsers] = useState(true);
      const handleUsersClick = () => {
        setShowUsers(true);
      };
    
    const handlePostsClick = () => {
        setShowUsers(false);
      };
    const handleOptionChange = (e) => {
        setSelectedOption(e.target.value);
    };
    const singlePost = async (post) => {
        await setPost(post);
        console.log(post);
        navigate('/singlepost');
        return;
    }
    const singleUser = async (user) => {
        await setUser(user);
        navigate('/singleuser');
        return;
    }
  return (
    <div className="search-component">
      <div className="search-options">
        <button
          className={`search-option-button ${showUsers ? 'active' : ''}`}
          onClick={handleUsersClick}
        >
          Users
        </button>
        <button
          className={`search-option-button ${!showUsers ? 'active' : ''}`}
          onClick={handlePostsClick}
        >
          Posts
        </button>
      </div>
      <div className="search-sections">
        {showUsers ? (
          <div className="search-section search-section-left">
            <h2>Users</h2>
            {searchResult[1]?.map((user)=><div key={user.id} onClick={()=>singleUser(user)} className='separator'><User user = {user} /></div>)}
          </div>
        ) : (
          <div className="search-section search-section-right">
            <div className = "radio">
            <label className="search-option1">
          <input
            type="radio"
            name="searchOption"
            value="1"
            checked = {selectedOption === "1"}
            onChange={handleOptionChange}
          />
          By Content
        </label>
        <label className="search-option2">
          <input
            type="radio"
            name="searchOption"
            value="2"
            checked={selectedOption === "2"}
            onChange={handleOptionChange}
          />
          By Topic
        </label>
        </div>
            {selectedOption === "1" && <div className="search-subsection1">
              <h2>Posts - Content</h2>
              {searchResult[0]?.map((post)=><div className='separatorpost' onClick={()=>singlePost(post)} key={post.idpost}><Postie post ={post} /></div>)}
            </div> } 
            {selectedOption === "2" && <div className="search-subsection2">
              <h2>Posts - Topic</h2>
              {searchResult[2]?.map((post)=><div key={post.idpost} onClick={()=>singlePost(post)} className='separatorpost'><Postie post ={post} /></div>)}
            </div>}
          </div>
        )}
      </div>
    </div>

  );
};

export default Search;
