import React, { useContext, useEffect, useState } from 'react';
import './Request.css'
import axios from 'axios';
import AuthContext from '../context/AuthProvider';

const Request = ({req}) => {
  const [user,setUser] = useState(null);
  const {auth} = useContext(AuthContext);
  useEffect(()=>{
  const takeUser = async () => {
    try{
      let response = await axios.get("http://88.200.63.148:5051/user?id="+req.fromId);
      setUser(response.data[0]);
      console.log(req);
    }
    catch(err){
      console.log(err);
    }
  };
  takeUser();
  },[]);
  const handleAccept = async (e) => {
    e.preventDefault();
    try{
      let response = await axios.post("http://88.200.63.148:5051/partnership",{uid:auth.id,pid:user.id});
      console.log(response);
      removeReq();
      var elem = document.getElementById("request"+req.idrequests);
      if(elem){
        elem.parentNode.removeChild(elem);
        elem.style.display = "none";}
    }
    catch(err){
      console.log(err);
    }
  }
  const removeReq = async() =>{ let response = await axios.post("http://88.200.63.148:5051/removereq",{toid:req.toId,fromid:req.fromId});
      console.log(response);
      console.log("request"+ req.idrequests);
      var elem = document.getElementById("request"+ req.idrequests);
      if(elem){
        elem.parentNode.removeChild(elem);
        elem.style.display = "none";}
      }
  const handleDecline = async (e) => {
    e.preventDefault();
    try{
      removeReq();
    }
    catch(err){
      console.log(err);
    }
  }
  return (
    <div className="request" id = {"request"+req.idrequests}>
      <div className="request-header">From: {user?.username}</div>
      <div className="request-description">requested a partnership </div>
      <div className="request-actions">
        <button onClick = {handleDecline} className="request-button">X</button>
        <button onClick = {handleAccept} className="request-button">✔</button>
      </div>
    </div>
  );
};

export default Request;
