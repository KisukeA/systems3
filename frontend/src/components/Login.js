import react from "react";
import { Link, useNavigate } from "react-router-dom";
import { useState } from "react";
import "./Login.css";
import axios from "axios";
import { useContext } from "react";
import AuthContext from "../context/AuthProvider";
import Nav from "./Nav.js";

const Login = () => {
    const navigate = useNavigate();
    const { setAuth } = useContext(AuthContext);
    const [error,setError] = useState(null);
    const [credentials, update] = useState({
        username: "",
        password: ""
    });
    const sendCred = async (e) => {  
        e.preventDefault();
        try{
            const a = await axios.post("http://88.200.63.148:5051/check", credentials,{
                withCredentials: true,
              });
              console.log(a);
            setAuth(a.data);     
            navigate("/profile");
        }
        catch(err){
            console.log(err);
            setError(err.response?.data);
           }
    }
    const updateC = (e) => {
        update((previous)=>({...previous, [e.target.name]: e.target.value}));
    }
    console.log(credentials);
    return (
        <div>
            <h1>Login cuzz</h1>
             <Nav></Nav>
             <div className="login-container">
                <h2>Login</h2>
                <form>
                    <div className="form-group">
                    <label htmlFor="username">Username:</label>
                    <input onChange = {updateC} type="text" id="username" name="username" placeholder="Enter your username"></input>
                    </div>
                    <div className="form-group">
                    <label htmlFor="password">Password:</label>
                    <input onChange = {updateC} type="password" id="password" name="password" placeholder="Enter your password"></input>
                    </div>
                    <div style = {{color:"red"}}>{error && error}</div>
                    <div>Don't have an account? <Link to ="/signup">Sign up.</Link></div>
                    <div>Or join as <Link to ="/home">guest.</Link></div>
                    <div>
                    <button onClick={sendCred} className = "submitt" >Login</button>
                    </div>
                    
                </form>
            </div>
        </div>
        )
}

export default Login