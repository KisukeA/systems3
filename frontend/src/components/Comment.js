import { useContext, useState } from "react";
import AuthContext from "../context/AuthProvider";
import { useNavigate } from "react-router-dom";
import axios from "axios";

const Comment = ({data}) =>{
    const [user, setUserC] = useState(null);
    const { setUser, available } = useContext(AuthContext);
    const navigate = useNavigate();
    const singleUser = async (user) => {
        try{
            const response = await axios.get("http://88.200.63.148:5051/user?id="+data.author_id);
            setUser(response.data);
            navigate('/singleuser');
            return;
        }
        catch(err){
            console.log(err);
        }
    }
    return (
        <div className="comments">
        <div className="comment">
        <div className="comment-header">
        <img src="user-avatar.jpg" onClick={available.includes(data.author_id) ? singleUser : ()=>{}} alt="User Avatar" className="comment-avatar"></img>
        <h3 className="comment-author">{data.author_id}</h3>
        </div>
        <p className="comment-content">{data.content}</p>
        </div>
        </div>

    )
}
export default Comment;