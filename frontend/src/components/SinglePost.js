import "./SinglePost.css";
import AuthContext from '../context/AuthProvider';
import { useContext } from 'react';
import Post from "./Post.js";
import { Link } from 'react-router-dom';

const SinglePost = ({post}) => {
    useContext(AuthContext);
    console.log(post);
    return (
        <div className="decorate">
            <Link to="javascript:void(0)" onClick={() => window.history.back()}><button style = {{backgroundColor:"black",height:"50px",width:"100px",color:"white",borderRadius:"10px"}}>Go back</button></Link>
            <Post post = {post}/>
        </div>
    )
}
export default SinglePost;