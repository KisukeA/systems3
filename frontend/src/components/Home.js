import React from "react";
import Nav from "./Nav";
import "./Home.css";
import Background from '../ptbg.jpg';
import { useEffect, useState, useContext } from "react";
import axios from "axios";
import  Post  from "./Post.js";
import Cookies from 'js-cookie';
import AuthContext from "../context/AuthProvider";
import { Link } from "react-router-dom";

const Home = () => {
    const { auth, } = useContext(AuthContext);
    const [posts, setPosts] = useState([]);
    useEffect(() => {
      // Fetch posts from the database 
      const fetchPosts = async (req,res) => {
        try {
          //console.log(auth.id);
          const response = await axios.get("http://88.200.63.148:5051/posts?id="+[ auth != null ? auth.id : -1]);
          console.log(response);
          setPosts(response.data);
        } catch (error) {
          console.error('Error fetching posts:', error);
        }
      };
      fetchPosts();
    }, []);
    let {available} = useContext(AuthContext);
    console.log(available);
  return (
    <div>
        <div className = "blur" style = {{backgroundColor: "rgba(0, 0, 0, 1)",backgroundSize: "cover",backgroundPosition: "center",backgroundRepeat:"no-repeat",backgroundImage:"url(" + Background + ")"}}>
        <h1 style = {{width:"9vw",textAlign:"right", color:"white",margin:"0",height:"30px",padding:"2vh",paddingRight:"8vw",lineHeight: "30px"}}>Kone</h1>
        </div>      
        <Nav/>
      <div className="home-container">
      <div className="sidebar">
          
            {auth!=null &&
            <div className="profile-link">
            <h3>Profile</h3>
            <p>View your profile</p>
            <Link to = "/profile"><button>Go to Profile</button></Link>
            </div>}
        
        </div>
        <div className="timeline">
          <h2>Timeline</h2>
          {/* Timeline content goes here */}       
          {/*{posts.map((post) => <Post key = {post.idpost} post = {post}></Post>)}*/}
          {posts.map((post) => <Post post = {post} key = {post.idpost}/>)}
        </div>
        
      </div>
    </div>
  );
};

export default Home;
