import React, { useContext, useRef } from "react";
import { Link } from "react-router-dom";
import "./Nav.css";
import  { useEffect,useState } from 'react'
import { AuthContext } from "../context/AuthProvider";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSearch } from '@fortawesome/free-solid-svg-icons';
import axios from "axios";
import { useNavigate } from "react-router-dom";
import Search from "./Search.js";

const Nav = () => {
  const dropdownRef = useRef(null);
  const [isDropdownOpen, setIsDropdownOpen] = useState(false);
  const [searchResult, setSearchResult] = useState([]);
  const navigate = useNavigate();
  const [noti,setNoti] = useState(false);
  const [isSearch,setIsSearch] = useState(false);
  var currentPage = window.location.pathname; 
  const {auth} = useContext(AuthContext);
  const [isLogged, setIs] = useState(false);
  const [searchData, setSearchData] = useState("");
  useEffect(() => {
    if(auth==null){
      setIs(false);
    }
    else{
      setIs(true);
    }

    if(currentPage == '/home'){
        setIsSearch(true);
    }
    else{
        setIsSearch(false);
    }
    currentPage = window.location.pathname;
  });
  const handleClickOutside = (event) => {
    if (dropdownRef.current && !dropdownRef.current.contains(event.target)) {
      setIsDropdownOpen(false);
    }
  };
  window.addEventListener('scroll', function() {
    var navbar = document.getElementById('navbar');
    var navbarOffsetBottom = navbar?.offsetHeight;
    if (window.pageYOffset >= navbarOffsetBottom) {
      navbar?.classList.add('fixed');
    } else {
      navbar?.classList.remove('fixed');
    }
  });
  useEffect(() => {
    document.addEventListener('click', handleClickOutside);
    return () => {
      document.removeEventListener('click', handleClickOutside);
    };
  }, []);
  const handleSearch = async (e) => {
      e.preventDefault();
      setSearchData("");
      if(searchData==null || !searchData.trim()){return;}
      try {
        setSearchResult([]);
         const response = await axios.get("http://88.200.63.148:5051/search?data="+searchData);
         const allEmpty = response.data.every((arr) => arr.length === 0);
         console.log(response.data);
         if(allEmpty){
          setNoti(true);
          setTimeout(() => {
            setNoti(false);
          }, 2000);
         }
         else {
          setNoti(false);
          setIsDropdownOpen(true);
          setSearchResult(response.data);
          //navigate('/search');
        } 
      }
      catch(err){
        console.log(err);
      }
  }
  const handleSearchChange = (e) => {
    setSearchData(e.target.value);
  }
  return (
    <nav className="nav" id="navbar">
      <ul>
        <li>
          <Link className="hoverable" style = {{color:"white"}} to="/"><a>Home</a></Link>
        </li>
        <li>
          <Link className="hoverable" style = {{color:"white"}} to="/about"><a>About</a></Link>
        </li>
        <li>
            <Link className="hoverable" style = {{color:"white"}} to = "/signup"><a>Sign up</a></Link>
        </li>
        <li>
            {!isLogged ? (
            <Link className="hoverable" style = {{color:"white"}} to = "/login"><a>Log in</a></Link>)
            : <Link className="hoverable" style = {{color:"white"}} to = "/logout"><a>Log out</a></Link>
            }
        </li>
        <li>
        {
          isSearch &&  
        <div className="dropdown" ref={dropdownRef}>
        <div className="search-barnav">
        <input id = {noti ? 'not-found' : ''} className="search-input" value = {searchData} onChange={handleSearchChange} placeholder= {noti ? "No results found" : "Search"} />
        <FontAwesomeIcon onClick = {handleSearch} icon={faSearch} className="search-icon" />
        </div>
          {isDropdownOpen && 
            <div className="dropdown-contentnav">
            <Search searchResult = {searchResult}/>
            </div>
          }        
        </div>
        }
        </li>
      </ul>
    </nav>
  );
};
export default Nav;
