import axios from "axios";
import React, { useState, useEffect, useContext } from "react";
import { AuthContext } from "../context/AuthProvider.js";
import { useNavigate } from "react-router-dom";

const Logout = () => {
    const navigate = useNavigate();
    const { setAuth, setAvailable } = useContext(AuthContext);
    const [isVisible, setIsVisible] = useState(true);
    useEffect(() => {
        const timer = setTimeout(() => {      
            setIsVisible(false);  
            navigate("/"); 
        }, 2000);
        const func = async () => {
            try{
                const response = await axios.post("http://88.200.63.148:5051/logout",{withCredentials: true});
                console.log(response);
            }
            catch(err){
                console.log(err);
            }
        }
        setAuth(null);
        func();      
        return () => {setAvailable([]); clearTimeout(timer)};
  }, []);
    return (
        <div>
         {isVisible &&   
        <div style = {{height:"20px",backgroundColor:"blue",color:"white"}}>you have been logged out</div>}
        </div>
    )
}
export default Logout;