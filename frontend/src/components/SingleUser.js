import React from 'react';
import './SingleUser.css';
import AuthContext from '../context/AuthProvider';
import { useContext } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';

const SingleUser = () => {
    const { user, auth, available } = useContext(AuthContext);
    const reqPartner = async (e) => {
        e.preventDefault();
        console.log(user.id);
        console.log(available);
        try{
            if(available.includes(user.id)){
                const a = await axios.post("http://88.200.63.148:5051/requests", [user.id,auth.id]);
                console.log(a.data);
            }
        }
        catch(err){
            console.log(err);
           }
    }
  return (
    <div className="singleuserprofile-page">
      <div className="singleuserprofile-header">
        <img src={user?.ppic} alt="Profile Avatar" className="singleuseravatar" />
        <h2 className="singleuserusername">{user?.username}</h2>
      </div>
      <div className="singleuserprofile-content">
        <h3>About Me</h3>
        <p>{user?.about}</p>
        <h3>Interests</h3>
        <ul>
          <li>a</li>
          <li>b</li>
          <li>c</li>
        </ul>
      </div>
      <button className = "reqPartner" onClick = {reqPartner}>Request partner up</button>
      <Link to="javascript:void(0)" onClick={() => window.history.back()}><button style = {{backgroundColor:"black",color:"white",borderRadius:"10px"}}>Go back</button></Link>
    </div>
    
  );
};

export default SingleUser;
