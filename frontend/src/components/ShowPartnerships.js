import React, { useState,useEffect, useContext } from 'react';
import './ShowPartnerships.css';
import axios from 'axios';
import AuthContext from '../context/AuthProvider';
import Partnership from './Partnership.js';
import { Link } from 'react-router-dom';


const ShowPartnerships = () => {
const {childCount, setChildCount} = useContext(AuthContext);
  const [partnerships,setP] = useState([]);
  const {auth} = useContext(AuthContext);
    const fetchPartnerships = async () => {
        try {
            //console.log(auth.id);
            const response = await axios.get("http://88.200.63.148:5051/partner?id="+[ auth != null ? auth.id : -1]);
            console.log(response);
            setP(response.data);
            setChildCount(response.data.length);
          } catch (error) {
            console.error('Error fetching posts:', error);
          }
      };
      useEffect(()=>{
        fetchPartnerships();
      },[]);
      useEffect(()=>{
        fetchPartnerships();
      },[childCount]);
  return (
    <div className="partnership-list">
      <h2>Partnerships</h2>
      <p className="partnership-count" id = "partnership-count">{partnerships.length} partnerships</p>
      <div className="partnership-container"id="partnership-container">
        {partnerships.map(partnership => (
          <Partnership key={partnership.idPartnership} partnership={partnership} />
        ))}
      </div>
      <Link to="javascript:void(0)" onClick={() => window.history.back()}><button style = {{backgroundColor:"black",color:"white",borderRadius:"10px"}}>Go back</button></Link>
    </div>
  );
};
export default ShowPartnerships;
