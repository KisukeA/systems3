import React, { useState,useEffect, useContext } from 'react';
import './Partnership.js'
import axios from 'axios';
import AuthContext from '../context/AuthProvider.js';

const Partnership = ({partnership}) => {
    const [partner,setMyPartner] = useState({});
    const {childCount,setChildCount} = useContext(AuthContext);
    const fetchPartner = async () => {
        try{
            const response = await axios.get("http://88.200.63.148:5051/user?id="+partnership.partner_id);
            console.log("response is");
            console.log(response);
            setMyPartner(response.data[0]);;
        }
        catch(err){
            console.log(err);
        }    
    };
    const delPart = async () => {
        try{
            const response = await axios.post("http://88.200.63.148:5051/deletepart",{id:partnership.idPartnership});
            console.log("response is");
            console.log(response);
            var elem = document.getElementById("partnership"+partnership.idPartnership);
            setChildCount((prev)=>prev-1);
            elem.parentElement.removeChild(elem); 
            window.location.reload(); 
        }
        catch(err){
            console.log(err);
        }    
    }
    useEffect(()=>{
        fetchPartner();
    },[]);
    useEffect(()=>{
        console.log(partner);
    },[]);
  return (
    <div className = "partnership" id={"partnership"+partnership.idPartnership}>
      <img className="partnership-logo" src={'frontend\\src\\gin.jpeg'} alt={`${partnership.name} logo`} />
      <div className="partnership-info">AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
        <h2 className="partnership-name">{partnership.name}{partnership.idPartnership}</h2>
        <p className="partnership-description">blablalbal</p>
        <div>Buddy is : {partner?.username} {partner?.id}</div>
      </div>
      <button onClick = {delPart} style = {{cursor:"pointer",width:"50px", height:"20px",backgroundColor:"black",color:"white"}}>Delete</button>
    </div>
  );
};

export default Partnership;
