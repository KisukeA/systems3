import React from 'react';
import './Postie.css';
import AuthContext from '../context/AuthProvider';
import { useContext,useEffect, useState } from 'react';

import axios from 'axios';

const Postie = ({ post }) => {
  const [comments,setComments] = useState([]);
  const fetchComments = async () => {
    try {
      const url = "http://88.200.63.148:5051/comments?postId=" + post.idpost;
      const response = await axios.get(url);
      setComments(response.data);
    } catch (error) {
      console.error('Error fetching comments:', error);
    }
  };
  useEffect(()=>{
    fetchComments();
  },[]);
  return (
    <div className="postie-container">
      <div className="postie">
        <div className="postie-header">
          <h3 className="postie-title">{post.title}</h3>
          <p className="postie-author">{post.author_id}</p>
        </div>
        <div className="postie-footer">
          <p className="postie-likes">{post.shortDescription} </p>
          <p className="postie-comments">Comments:{comments.length} </p>
        </div>
      </div>
    </div>
  );
};

export default Postie;
