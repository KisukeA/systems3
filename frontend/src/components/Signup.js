import react from "react"
import { Link } from "react-router-dom"
import { useState } from "react";
import "./Signup.css";
import axios from "axios";
import Nav from "./Nav.js";

const Signup = () => {
    const [success, setSuccess] = useState(false);
    const [error,setError] = useState(null);
    const [info, setInfo] = useState({
        username: "",
        password: "",
        confirm: "",
    })
    const updInfo = (e) =>{
        setInfo((previous)=>({...previous,[e.target.name]:e.target.value}));
        setSuccess(false);
    }
    console.log(info);
    const sendRegister = async (e) => {
        e.preventDefault();
        try{
            const a = await axios.post("http://88.200.63.148:5051/register", info);
            setSuccess(true);
            console.log("abe");
            console.log(a);
        }
        catch(err){
            console.log("odovde")
            console.log(err);
            setError(err.response.data);
        }
    }
    return (
        <div>
            <h1>Sign up</h1>
            <Nav></Nav>
            <div className="container">
                <h2>Sign Up</h2>
                <form>
                <div className="form-group">
                <label htmlFor="username">Username</label>
                <input onChange = {updInfo} type="text" id="username" name="username" placeholder="Enter your username" required></input>
                </div>
                <div className="form-group">
                <label htmlFor="password">Password</label>
                <input onChange = {updInfo} type="password" id="password" name="password" placeholder="Enter your password" required></input>
                </div>
                <div className="form-group">
                <label htmlFor="confirmPassword">Confirm Password</label>
                <input onChange = {updInfo} type="password" id="confirmPassword" name="confirm" placeholder="Confirm your password" required></input>
                </div>
                <div style = {{color:"red"}}>{error && error}</div>
                {success && <span>Successfully created an account <Link style = {{textDecoration:"none"}} to = "/login">Log in.</Link></span>}
                <div>
                <button onClick={sendRegister} className="btn" style = {{marginTop:"15px"}}>Sign Up</button>
                </div>
                </form>
                </div>
        </div>)
        
}

export default Signup