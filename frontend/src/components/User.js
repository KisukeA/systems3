import React from 'react';
import './User.css';

const User = ({ user }) => {
  return ( 
    <div className="user">
      <img src={user.ppic} alt="Profile" className="profile-picture" />
      <div className="user-details">
        <h3 className="username">{user.username}</h3>
        <div className="additional-info">
          {/* Additional information goes here */}
        </div>
      </div>
    </div>
  );
};

export default User;
