import react from "react";
import { useState,useEffect, useContext } from 'react';
import axios from "axios";
import { Link } from "react-router-dom";
import "./Welcome.css";
import bakugo from "../bakugo.jpeg";
import kid from "../kid.jpeg";
import gin from "../gin.jpeg";
import Nav from "./Nav.js";
import AuthContext from "../context/AuthProvider";

const Welcome = () => {
  const { setAuth,setAvailable } = useContext(AuthContext);
    return (  
        <div>
          <h1>Welcome</h1>
          <header>
          <Nav/>
          </header>
  
  <section className="hero">
    <div className="hero-content">
      <h1>Peter on a Buttis</h1>
      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
      <a href="#" className="btn">Get Started</a>
    </div>
  </section>
  
  <section className="features">
    <h2>Key Features</h2>
    <div className="feature-card">
      <img src={gin} alt="Feature 1"></img>
      <h3>Feature 1</h3>
      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
    </div>
    <div className="feature-card">
      <img src={kid} alt="Feature 2"></img>
      <h3>Feature 2</h3>
      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
    </div>
    <div className="feature-card">
      <img src={bakugo} alt="Feature 3"></img>
      <h3>Feature 3</h3>
      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
    </div>
  </section>
  
  <footer>
    <p>&copy; 2023 Fancy Website. All rights reserved.</p>
  </footer>
        
        </div>
        
    )
}

export default Welcome