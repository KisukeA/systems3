import React from "react";
import Nav from "./Nav.js";
import "./About.css";

const About = () => {
  return (
    <div>
        <h1 className="about-heading">About Us</h1>
        <header>
        <Nav></Nav>
        </header>
      
      <div className="about-container">
        
        <p className="about-description">
          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed eu
          consectetur lorem. Aliquam feugiat urna turpis, in euismod neque
          tincidunt at. Integer fringilla odio et mi condimentum, at dapibus
          justo tristique. Mauris sed feugiat enim. Duis nec est non purus
          pharetra mattis. Suspendisse suscipit ullamcorper elit, ut
          consectetur turpis lobortis nec. Etiam tincidunt malesuada leo, vitae
          congue neque facilisis in. Vivamus nec sollicitudin elit. Mauris
          tincidunt ullamcorper orci, vitae fringilla elit interdum ac.
        </p>
      </div>
    </div>
  );
};

export default About;
