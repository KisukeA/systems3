import React, { useContext, useEffect } from "react";
import "./Post.css";
import Comment from "./Comment";
import { useState } from "react";
import axios from "axios";
import AuthContext from "../context/AuthProvider";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faEllipsisV } from "@fortawesome/free-solid-svg-icons";

const Post = ({post}) => {
  const [comments, setComments] = useState([]);
  const { auth } = useContext(AuthContext);
  const { setAvailable } = useContext(AuthContext);
  const fetchComments = async () => {
    try {
      const url = "http://88.200.63.148:5051/comments?postId=" + post.idpost;
      const response = await axios.get(url);
      setComments(response.data);
      const filteredArray = response.data.map(obj => obj.author_id);
      if(post.author_id == auth?.id){
        setAvailable((previous)=>(Array.from(new Set([...previous,...filteredArray]))));//setting the available users for partnership
      }
    } catch (error) {
      console.error('Error fetching comments:', error);
    }
  };
  const [commentData, setComm] = useState("");
  const commChange = (e) => {
    setComm(e.target.value);
  }
  const addComm = async (e) => {
    e.preventDefault();
    if(commentData==null || !commentData.trim()) return;
    try{
      const response = await axios.post("http://88.200.63.148:5051/comment",{comment:commentData,user:auth.id,post:post.idpost});
      
      fetchComments();
      setComm("");
      console.log(response);
    }
    catch(err){
      console.log(err);
    }
  }
  useEffect(() => {
    fetchComments();
  }, []);
  const toggleComments = (a) => {
    const clas = "post"+a+"-comments";
    const clas2 = "post-add-comment"+a;
    var commentsSection = document.getElementById(clas);
    var addcomment = document.getElementById(clas2);
    commentsSection.classList.toggle('show');
    addcomment.classList.toggle('show');
  }
  const [user,setUser] = useState(null);
  useEffect(()=>{
  const takeUser = async () => {
    try{
      let response = await axios.get("http://88.200.63.148:5051/user?id="+post.author_id);
      setUser(response.data[0]);
      console.log(response);
    }
    catch(err){
      console.log(err);
    }
  };
  takeUser();
  },[]);
    return(
      <div className="post">
  <div className="post-header">
    <div className="post-profile">
      <img src="user-avatar.jpg" alt="User Avatar" className="post-profile-picture" />
      <h3 className="post-author">Author: {user?.username}</h3>
    </div>
    <div className="post-options">
      <FontAwesomeIcon  icon={faEllipsisV}/>
    </div>
  </div>
  <div className="post-content">
    <h2 className="post-title">Post {post.idpost}</h2>
    <div className="post-metadata">
      

    </div>
    <p className="posts-content">{post.content}</p>
  </div>
  <div className="post-footer">
    <div className="post-actions">
      <button className="post-like">Like</button>
      <button onClick={() => toggleComments(post.idpost)} className="post-comment">Comment</button>
      <span className="post-comments-label" onClick={() => toggleComments(post.idpost)}>{comments.length} comments</span>
    </div>
    <div id={"post" + post.idpost + "-comments"} className="post-comments">
      {comments.map((comment) => (
        <Comment key={comment.idComment} data={comment} />
      ))}
    </div>

    </div>
    <div id={"post-add-comment" + post.idpost} className="post-add-comment">
      {auth !== null && (
        <div className="condWrapper">
          <input type="text" className="comment-input" value={commentData} onChange={commChange} placeholder="Write a comment..." />
          <button onClick={addComm} className="add-comment-button">Submit</button>
        </div>
      )}
    
  </div>
</div>

    )
}
export default Post;