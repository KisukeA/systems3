import "./Profile.css";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTwitter,faFacebookF, faInstagram } from '@fortawesome/free-brands-svg-icons';
import { faEnvelope, faPhone, faSearch } from '@fortawesome/free-solid-svg-icons';
import {Link, useNavigate} from "react-router-dom";
import AuthContext from "../context/AuthProvider";
import {useState, useContext, useEffect, useRef} from "react";
import axios from "axios";
import Post from "./Post";
import Search from "./Search";
import Request from "./Request";

const Profile = () => {
    const navigate = useNavigate();
    const [option,setOpt] = useState("call");
    const [topicData, setTopic] = useState("");
    const [descData, setDesc] = useState("");
    const [requests, setRequests] = useState([]);
    const [isOpen, setIsOpen] = useState(false);
    const dropdownRef1 = useRef(null);
    const dropdownRef2 = useRef(null);
    const { available, auth } = useContext(AuthContext);
    const dropdownRef = useRef(null);
    const [isDropdownOpen, setIsDropdownOpen] = useState(false);
    const [postData,setPostdata] = useState("");
    const [myPosts , setMyPosts ] = useState([]);
    const [searchData, setSearchData] = useState("");
    const [showNotification, setShowNotification] = useState(false);
    const [showSearch, setShowSearch] = useState(false);
    const [searchResult, setSearchResult] = useState([]);
    const [empty,setEmpty] = useState(false);
    const handleClickOutside = (event) => {
      if (dropdownRef.current && !dropdownRef.current.contains(event.target)) {
        setIsDropdownOpen(false);
      }
    };
    const handleClickOutside1 = (event) => {
      if ((dropdownRef1.current && !dropdownRef1.current.contains(event.target)) && (dropdownRef2.current && !dropdownRef2.current.contains(event.target))) {
        setIsOpen(false);
      }
    };
    const handleDropdownClick = async () => {
      setIsOpen(true);
      if (requests.length == 0){
        setEmpty(true);
      }
      
    };
    const handleOptionChange = (e)=>{
      setOpt(e.target.value);
    }
    useEffect(() => {
      document.addEventListener('click', handleClickOutside);
      return () => {
        document.removeEventListener('click', handleClickOutside);
      };
    }, []);
    useEffect(() => {
      document.addEventListener('click', handleClickOutside1);
      return () => {
        document.removeEventListener('click', handleClickOutside1);
      };
    }, []);
    const fetchMyPosts = async () => {
        try{
            const response = await axios.get("http://88.200.63.148:5051/myposts?id="+auth.id);
            console.log("response is");
            console.log(response.data);
            setMyPosts(response.data);
            setPostdata("");
        }
        catch(err){
            console.log(err);
        }    
    };
    const changePost = (e) =>{
        setPostdata(e.target.value);
        console.log(postData);
    }
    const submitPost = async (e) => {
        e.preventDefault();
        setShowNotification(true);
        if(postData==null || !postData.trim()) return;
        try{
            const resp = await axios.post("http://88.200.63.148:5051/addpost",{topic:topicData, author_id:auth.id, short:descData, content:postData, type:option});
            console.log(resp);
            setTimeout(() => {
                setShowNotification(false);
            }, 3000);
            setDesc("");
            setTopic("");
            fetchMyPosts();
        }
        catch(err){
            console.log(err);
        }
    }
    const handleSubmit = async (e) =>{
      e.preventDefault();
      setSearchData("");
      if(searchData==null || !searchData.trim()){return;}
      try {
         const response = await axios.get("http://88.200.63.148:5051/search?data="+searchData);
         const allEmpty = response.data.every((arr) => arr.length === 0);
         console.log(response.data);
         if(allEmpty){
          //setShowNotification(true);
          setShowSearch(true);
          setTimeout(() => {
            //setShowNotification(false);
            setShowSearch(false);
          }, 2000);
         }
         else {
          //setShowNotification(false);
          setIsDropdownOpen(true);
          setShowSearch(false);
          setSearchResult(response.data);
          //navigate('/search');
        } 
      }
      catch(err){
        console.log(err);
      }
    }
    const handleInputChange = (e) =>{
      setSearchData(e.target.value);
    };
    useEffect(() =>{
        fetchMyPosts();      
    },[]);
    console.log(available);
    const fetchRequests = async () => {
      try {
        console.log(auth.id);
        const response = await axios.get("http://88.200.63.148:5051/request?data="+auth.id);
        console.log("eoo");
        console.log(response);
        setRequests(response.data);
      }
      catch(err){
        console.log(err);
      }
  }
  useEffect(() =>{
    fetchRequests();
},[]);
useEffect(() =>{
  console.log(requests);
},[requests]);
  const changeShort = (e) =>{
    setDesc(e.target.value);
  };
  const changeTopic = (e) => {
    setTopic(e.target.value);
  }
    return (
        <div className="profile-page">
  <div className="sidebar-left">
    <div className="sidebar-section">
      <h3 className="sidebar-title">Section 1</h3>
    </div>
    <div className="sidebar-section">
      <h3 className="sidebar-title">Section 2</h3>
    </div>
    <div className="sidebar-section">
      <h3 className="sidebar-title">Section 2</h3>
    </div>
  </div>

  <div className="main-content">
    <div className="header">
      <h1 className="website-name">My Website</h1>
      <div className="search-bar"ref={dropdownRef}>      
          <div className="encapsule">
          <input id = {showSearch ? 'not-found' : ''} className="search-input" value = {searchData} placeholder= {showSearch ? "No results found" : "Search..."}  onChange={handleInputChange} />         
            <FontAwesomeIcon className = "search-icon" onClick = {handleSubmit} icon={faSearch} /></div>          
            {isDropdownOpen && 
            <div className="dropdown-contentProfile">
            <Search searchResult = {searchResult}/>
            </div>
          }  
      </div>
         
    </div>
    <div className="bubble">
    <div className="profile-header">
        <img src="profile-picture.jpg" alt="Profile Picture" className="profile-picture"></img>
        <h1 className="profile-name">{auth.username}</h1>
        <p className="profile-bio">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam facilisis tellus eget massa aliquet, sed dapibus magna tempus.</p>
      </div>

      <div className="profile-content">
        <div className="contact-bar">
          <div className="contact-item">
            <FontAwesomeIcon icon={faFacebookF} />
            <span className="contact-data">Facebook</span>
          </div>
          <div className="contact-item">
            <FontAwesomeIcon icon={faTwitter} />
            <span className="contact-data">Twitter</span>
          </div>
          <div className="contact-item">
            <FontAwesomeIcon icon={faInstagram} />
            <span className="contact-data">Instagram</span>
          </div>
          <div className="contact-item">
            <FontAwesomeIcon icon={faEnvelope} />
            <span className="contact-data">Email</span>
          </div>
          <div className="contact-item">
            <FontAwesomeIcon icon={faPhone} />
            <span className="contact-data">Phone</span>
          </div>
        </div>
      </div>
    </div>
    <div className="new-post-container">
        <textarea className="new-post-input" value = {postData} onChange = {changePost} placeholder="Write your post..."></textarea>
        <textarea className = "Shortdesc" value = {descData} onChange={changeShort} placeholder = "Short Description(optional)"></textarea>
        <textarea className = "Topic" value = {topicData} onChange={changeTopic} placeholder = "Topic(optional)"></textarea>
        <label className="ssearch-option1">
          <input
            type="radio"
            name="searchOption"
            value="call"
            checked = {option === "call"}
            onChange={handleOptionChange}
          />
          call
        </label>
        <label className="ssearch-option2">
          <input
            type="radio"
            name="searchOption"
            value="query"
            checked={option === "query"}
            onChange={handleOptionChange}
          />
          query
        </label>
        <button className="submit-button" onClick = {submitPost} >Submit</button>
        {showNotification && (
        <div className="notification">
          <p>Post has been added!</p>
        </div>
      )}
        </div>
    <div className="posts"> {myPosts?.map((post)=> <div key={post.idpost} className="bubble">
      <Post  post={post}></Post>
    </div>)} 
    </div>
  </div>

  <div className="sidebar-right">
    <Link to="/home">
    <button>Timeline</button>  
    </Link>
    <Link to = "/showpart" style={{textDecoration:"none"}}><div className="bubble-section">

      <h3 className="sidebar-title">Partnerships</h3>
      
    </div></Link>
    <div className="bubble-section" onClick={handleDropdownClick} ref={dropdownRef1}>
      <h3 className="sidebar-title">Requests</h3>
    </div>
    {isOpen && (
        <div className="dropdown-content"ref={dropdownRef2} >
          {empty && <div>No requests yet.</div>}
          {requests?.map((req)=>(<div key = {req.idrequests}><Request req = {req}/></div>))}
        </div>
      )}
  </div>
</div>
    )
}
export default Profile;